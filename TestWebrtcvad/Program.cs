﻿namespace TestWebrtcvad
{
    using System;
    using System.Runtime.InteropServices;


    /// <summary>
    ///     The program.
    /// </summary>
    public class Program
    {
        [DllImport("webrtcvad.dll")]
        public static extern int vad_create();

        [DllImport("webrtcvad.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void vad_free(int handle);

        [DllImport("webrtcvad.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int vad_init(int handle);

        [DllImport("webrtcvad.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int vad_process(int handle, int fs, short[] buf, int frame_length);

        [DllImport("webrtcvad.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int vad_set_mode(int handle, int mode);

        [DllImport("webrtcvad.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern bool valid_rate_and_frame_length(int rate, int frame_length);

        [DllImport("webrtcvad.dll")]
        public static extern int run();


        private static void Main(string[] args)
        {
            int res = run();
            Console.WriteLine("results: " + res); 
            Console.ReadKey();
        }
    }
}