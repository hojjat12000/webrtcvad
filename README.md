# Webrtc Voice Activity Detection (VAD) Library with PortAudio! 

This repository contains Webrtc Voice Activity Detection (**VAD**) static library with PortAudio and a C#.Net sample code. By calling the `run` function, this library listens to the default recording device for 15 seconds  (`NUM_MILI_SECONDS`) and during this time it will sends the captured noise to the VAD every 30ms (`DURATION`). VAD will process it and returns 1 if it detects any activity in the data.
 If 10 (`ACTIVE_TIME`) consecutive calls were 1, then the `run` function will return 1 which means there is a voice activity in the environment. Otherwise, the `run` function will return 0 for timeout (15 seconds passed without any activities), or -1 for error. There is a `C#` sample code included that calls the `run` function.
 Note that all of other useful WebrtcVAD functions are also available in the dll and accessible.

# Installation
There is the most recent version of Portaudio (as of 1/24/18) in this repository. Using VS2015 you can compile it. The files 

 - portaudio_x86.dll
 - portaudio_x86.lib
 - portaudio.h
 
are used in the Webrtc VAD code to compile `webrtc.dll`. This file and `portaudio_x86.dll` then are moved next to the executible file produced by the sample code to be used as shown here:

```
#!csharp
    [DllImport("webrtcvad.dll")]
    public static extern int run();
```

The installation is straightforward. There is no need to compile portaudio, since the binary files are included in the root of the `cpp` folder.

## Compile "portaudio"
In case you need to compile portaudio (for a different architecture, or for any other reasons) after downloading the code. You can compile portaudio by going to `cpp\portaudio\build\msvc` and opening `portaudio.sln`. The project has been successfully compiled using VS2015. There should be no problem compiling it, if there are any problems please visit [Portaudio website](http://portaudio.com/docs/v19-doxydocs/compile_windows.html). 
After the compilation, you can find the binary files in `cpp\portaudio\build\msvc\Win32\Debug` and the `.h` file in `cpp\portaudio\include`. 

## Compile "webrtc vad"
Open `webrtcvad.sln` in the root of the repository (the project is built using VS2015). `portaudio.lib` and `portaudio.h` should be in the `Project>Properties` in the appropriate folders (the config files are setup to be portable, so current `.sln` file should contain the required settings.
Compile the code and run `TestWebrtcvad`. In the output you should see 1s for every 30ms of activity captured from your default recording device.

## Tuning the Variables
There are a number of options in `microphone.h` that are tunable. Below are some of them:

 - SAMPLE_RATE: Microphone sample rate (16k by default)
 - DURATION: By default it is 30ms. It means every 30ms of recording is sent to VAD for processing.
 - WAIT_TIME: By default it is 100ms. It is the amount of time that the code waits and then it will check on the process and in case we can concluded that there is any activities in the voice, it will return finish the recording. So, the higher this wait time, the less responsive the library and the lower it is the more CPU usage. 
 - ACTIVE_TIME: By default it is 10. It is the number of consecutive positive samples to make us conclude that there is activity in the voice. Since each sample is 30ms (DURATION) hence, we need `10*30=300ms` of activity to declare that there is activity in the input.
 - NUM_MILI_SECONDS: By default 15000. It is the timeout value. It means that we will keep checking for 15 seconds, before we declare there is no activity. If you need continious activity detection, you should call the `run` function after each timeout. Increasing the value of NUM_MILI_SECONDS will increase the memory usage (since we allocate the memory for speed).
 - NUM_CHANNELS: It should be 1 since we use mono (not stereo) sound.
 - VAD_MODE: It can take four values: 0,1,2, and 3. The higher the value the more aggressive the VAD will be.  Read more [here](https://hackage.haskell.org/package/webrtc-vad-0.1.0.3/candidate/docs/Sound-VAD-WebRTC.html).
 
 ## Copyright
 
 Webrtc VAD belongs to Google (I think! For commercial use Google it). Portaudio is under MIT license (It's confusing, check it [here](http://www.portaudio.com/license.html)). The parts that I wrote are probably property of University of Denver contact me.