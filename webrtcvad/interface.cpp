 #include <stdio.h>
#include "webrtc/common_audio/vad/include/webrtc_vad.h"
#include "portaudio/microphone.h"
extern "C"
{
	__declspec(dllexport) void DisplayHelloFromDLL()
	{
		printf("Hello from DLL!\n");
	}

	__declspec(dllexport) int run()
	{
		return record();
	}

	__declspec(dllexport) int vad_create()
	{
		VadInst *handle;
		printf("Hello from DLL!\n");
		if (WebRtcVad_Create(&handle)) {
			return NULL;
		}
		return (int)handle;
	}

	__declspec(dllexport) void vad_free(int handle)
	{
		WebRtcVad_Free((VadInst*)handle);
	}

	__declspec(dllexport) int vad_init(int handle)
	{
		printf("Hello from init!\n");
		if (WebRtcVad_Init((VadInst*)handle)) {
			return NULL;
		}
		return 0;
	}

	__declspec(dllexport) int vad_set_mode(int handle, int mode)
	{
		if (mode < 0) {
			printf("Mode should be between 0-3!\n");
			return NULL;
		} else if (mode > 3) {
			printf("Mode should be between 0-3!\n");
			return NULL;
		}
		if (WebRtcVad_set_mode((VadInst*)handle, mode)) {
			printf("Mode did not change!\n");
			return NULL;
		}
		printf("Mode changed!\n");
		return 0;
	}

	__declspec(dllexport) bool valid_rate_and_frame_length(int rate, int frame_length)
	{
		if (rate > _CRT_INT_MAX) {
			return NULL;
		}
		if (frame_length > _CRT_INT_MAX) {
			return NULL;
		}
		if (WebRtcVad_ValidRateAndFrameLength(rate, frame_length)) {
			return false;
		} else {
			return true;
		}
	}

	__declspec(dllexport) int vad_process(int handle, int fs, const int16_t* buf, int frame_length)
	{
		int result;
		result =  WebRtcVad_Process((VadInst*)handle, fs, buf, frame_length);
		return result;
	}
}